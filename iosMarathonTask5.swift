class Parent {}

class Child1: Parent {}

class Child2: Parent {}

class House {
	var width: Int
	var height: Int

	func create() -> Int {
		return width * height
	}

	func destroy() {
		print("Дом уничтожен")
	}

	init(width: Int, height: Int) {
		self.width = width
		self.height = height
	}
} 

class Student {
    var name: String
    var avarageMark: Double
    var age: Int
    init(name: String, avarageMark: Int, age: Int) {
    	self.name = name
    	self.avarageMark = avarageMark
    	self.age = age
    }
}

var studentList = [
	Student(name: "Student1", avarageMark: 4.65, age: 24), 
	Student(name: "Student2", avarageMark: 4.21, age: 20),
	Student(name: "Student3", avarageMark: 3.76, age: 22)
]

class StudentSorting {
	static func sortByAvarageMark() {
		studentList = studentList.sorted(by: {$0.avarageMark > $1.avarageMark})
	}
	static func sortByAge() {
		studentList = studentList.sorted(by: {$0.age > $1.age})
	}
}

class SomeClass { }
// классы передаются по ссылке
// можно наследоваться от других классов
// есть функция deinit
struct SomeStruct { }
// наследование от протоколов
// передача по значению
